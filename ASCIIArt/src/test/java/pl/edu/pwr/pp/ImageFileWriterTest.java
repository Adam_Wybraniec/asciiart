package pl.edu.pwr.pp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
public class ImageFileWriterTest {
	
ImageFileReader imageReader;
ImageFileWriter imageFileWriter;	
	@Before
	public void setUp() {
		imageReader = new ImageFileReader();
		imageFileWriter = new ImageFileWriter();
	}
	
	
	@Test
	public void shouldWriteFileWithChars() {
	File resourcesDirectory = new File("src/test/resources");
	String fileName = resourcesDirectory.getAbsolutePath() + "/testAscii.txt";

	char ascii[][]=new char[32][8];
	for (int i=0;i<32;i++){
		for (int j=0;j<8;j++){
			ascii[i][j]='@';			
		}		
	}

	imageFileWriter.saveToTxtFile(ascii,fileName);
	
	char[][] asciitest = null;
	Path path= Paths.get(fileName);
	try (BufferedReader reader = Files.newBufferedReader(path)) {
		//String wczytanaLinia[];
		
		//wczytanaLinia=reader.readLine().split("");			
		int szerokosc = 8;
		int wysokosc = 32;				
	
		// inicjalizacja tablicy na odcienie szarości
		asciitest = new char[wysokosc][];

		for (int i = 0; i < wysokosc; i++) {
			asciitest[i] = new char[szerokosc];
		}

		// kolejne linijki pliku pgm zawierają odcienie szarości kolejnych
		// pikseli rozdzielone spacjami
		String line = null;
		int currentRow = 0;
		int currentColumn = 0;
		while ((line = reader.readLine()) != null) {
			char[] elements = new char[100];// = line.split('');
			for (int i = 0; i < line.length(); i++) {
				elements = line.toCharArray();
				asciitest[currentRow][currentColumn] = elements[i];
				currentColumn++;	
				//System.out.print(elements[i]);
				if(currentColumn==szerokosc){
					currentRow++;
					currentColumn=0;
					//System.out.println();
				}
			}		
		}		
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	for (int i=0;i<32;i++){
		for (int j=0;j<8;j++){
			assertThat(ascii[i][j], is(equalTo(asciitest[i][j])));
		}
	}
	}	
}
