package pl.edu.pwr.pp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.instanceOf;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.*;
public class RGBToGreyTest {
	

	ImageFileReader imageFileReader;
	@Mock
	File resourcesDirectory;
	ImageConverter imageConverter = new ImageConverter();
	@Mock
	String sciezka="";
	@Mock
	BufferedImage image;
	@Before
	public void setUp() {
		imageFileReader = new ImageFileReader();
		File filePath = new File("src/test/resources/Ok-icon.png");
		sciezka = filePath.getAbsolutePath();

	}

	@Test
	public void shouldReadSequenceFrom0To255() {
		try {
			image=ImageIO.read(new File(sciezka));
		}catch (IOException e) {
		}
		
		ImageConverter imageConverter = new ImageConverter();
		
		int [][] tablica = imageConverter.convertRGBToGreyScale(image);
		for (int[] row : tablica) {
			for (int intensity : row) {
				assertTrue(intensity<=255 && intensity>=0);
			}
		}
		
	}
	
	
	
	
}
