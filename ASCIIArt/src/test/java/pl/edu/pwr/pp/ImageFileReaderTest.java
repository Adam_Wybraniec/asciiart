package pl.edu.pwr.pp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.instanceOf;

import java.io.File;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ImageFileReaderTest {
	
	ImageFileReader imageReader;
	File resourcesDirectory;
	@Before
	public void setUp() {
		imageReader = new ImageFileReader();
		resourcesDirectory = new File("src/test/resources");
	}

	@Test
	public void shouldReadSequenceFrom0To255GivenTestImage() {
		// given
		
		String fileName = resourcesDirectory.getAbsolutePath() + "/testImage.pgm";
		// when
		int[][] intensities = null;
		try {
			intensities = imageReader.readPgmFile(fileName);
		} catch (URISyntaxException e) {
			Assert.fail("Should read the file");
		}
		// then
		int counter = 0;
		for (int[] row : intensities) {
			for (int intensity : row) {
				assertThat(intensity, is(equalTo(counter++)));
			}
		}
	}
/*	
	@Test
	public void shouldThrowExceptionWhenFileDontExist() {
		// given
		//File resourcesDirectory = new File("src/test/resources");
		//String fileName = resourcesDirectory.getAbsolutePath() + "/ewqewqnonexistent.pgm";
		String fileName = "dsadas.pgm";
		try {
			// when
			imageReader.readPgmFile(fileName);
			// then
			Assert.fail("Should throw exception");
		} catch (Exception e) {
			assertThat(e, is(instanceOf(NullPointerException.class)));
		}
		
	}*/
}
