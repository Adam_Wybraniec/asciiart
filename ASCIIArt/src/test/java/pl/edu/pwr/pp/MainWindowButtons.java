package pl.edu.pwr.pp;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.instanceOf;

import java.io.File;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
public class MainWindowButtons {
	@Mock
	MainWindow main;
	@Mock
	DialogWindow newWindow;
	@Before
	public void setUp() {
		main = new MainWindow();
		newWindow = new DialogWindow(main);
	}
	
	@Test
	public void ZapiszDoAsciButtonShouldBeNotActive() {
	newWindow.btnCancelClick();
	assertThat(main.btnZapiszAscii.isEnabled(),is(equalTo(false)));
	
	
	}
	
	
	
	
	
}
