package pl.edu.pwr.pp;

import java.io.PrintWriter;
//import java.io.File;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) {
		try {
			PrintWriter printWriter =new PrintWriter(fileName);
			for (int currentRow=0; currentRow<ascii.length; currentRow++){
				for (int currentColumn=0; currentColumn<ascii[0].length; currentColumn++){
					printWriter.print(ascii[currentRow][currentColumn]);
				}
				printWriter.println();				
			}	
		printWriter.close();
		}			
		catch (Exception ex){	
		}		
	}
}
