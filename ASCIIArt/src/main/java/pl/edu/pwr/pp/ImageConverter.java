package pl.edu.pwr.pp;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.net.URISyntaxException;
import java.util.stream.IntStream;

public class ImageConverter {

	/**
	 * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
	 * do białego (255).
	 */
	public static String INTENSITY_2_ASCII = "@%#*+=-:. ";
	public static String INTENSITY_2_ASCII_HIGH = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ";

	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilośsci znaków w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */
	
		static char intensityToAscii(int intensity,String jakosc) {
			
		double dzielnikNiska=25.6;
		double dzielnikWysoka=3.66;
		
		if (jakosc=="Niska") {
			double wynik=(intensity)/dzielnikNiska;
			int ktoryZnak=(int)wynik;
			return INTENSITY_2_ASCII.charAt(ktoryZnak);	
		}
		else {
			double wynik=(intensity)/dzielnikWysoka;
			int ktoryZnak=(int)wynik;
			return INTENSITY_2_ASCII_HIGH.charAt(ktoryZnak);	
		}

	}

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
		static char ascii[][]=null;
	public static char[][] intensitiesToAscii(int[][] intensities,String jakosc) {
			
		
		int columns=intensities[0].length;
		int rows=intensities.length;
		
		ascii = new char[rows][];

		for (int i = 0; i < rows; i++) {
			ascii[i] = new char[columns];
		}
		
		IntStream.range(0,rows).forEach(
				i -> ascii[i] = new char[columns]);
		
		for (int currentRow=0; currentRow<rows; currentRow++){
			
			for (int currentColumn=0; currentColumn<columns; currentColumn++){
			ascii[currentRow][currentColumn]=intensityToAscii(intensities[currentRow][currentColumn],jakosc);
			}
		}				
		return ascii;
	}

	
	int[][] convertRGBToGreyScale(BufferedImage image){
		int wysokosc = image.getHeight();
		int szerokosc = image.getWidth();
		int tablicaSzarosci[][]=new int[wysokosc][szerokosc];

		for (int i=0;i<wysokosc;i++){
			for (int j=0;j<szerokosc;j++){
				Color color = new Color(image.getRGB(j, i));
				int czerwony = color.getRed();
				int zielony = color.getGreen();
				int niebieski = color.getBlue();	
				double wynik = (0.2989*czerwony + 0.5870*zielony + 0.1140 * niebieski);
				tablicaSzarosci[i][j]=(int) wynik;
			}
		}
		return tablicaSzarosci;
	}	
	
	
	
	public BufferedImage handlePGMImage(String filePath,int[][] intensities){
		ImageFileReader imageFileReader = new ImageFileReader();
		BufferedImage image=null;
		try {
			intensities = imageFileReader.readPgmFile(filePath);
			int width=imageFileReader.szerokosc;
			int height=imageFileReader.wysokosc;
				
			image = new BufferedImage(width, height,BufferedImage.TYPE_BYTE_GRAY);
			
			WritableRaster raster = image.getRaster();
			for (int y = 0; y < height; y++) {
				for (int x = 0; (x < width); x++) {
					raster.setSample(x, y, 0, intensities[y][x]);
				}
			}	
			
			
			
		} catch (URISyntaxException e9) {
			System.out.println("niezwracam");
		}
			
		return image;
		
	}
	
	
	
	
	
	
}
