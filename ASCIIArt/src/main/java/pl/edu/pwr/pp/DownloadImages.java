package pl.edu.pwr.pp;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class DownloadImages {
	public 
		File resourcesDirectory = new File("src/main/resources");
	
	public String DownloadPGMFileFromURL(URL url) {
		String pathToSaveFile = resourcesDirectory.getAbsolutePath() + "/zarazusune.pgm";
		try {
			InputStream in = new BufferedInputStream(url.openStream());
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int n = 0;
			while (-1!=(n=in.read(buf)))
			{
			   out.write(buf, 0, n);
			}
			out.close();
			in.close();
			byte[] response = out.toByteArray();
			FileOutputStream fos = new FileOutputStream(pathToSaveFile);
			fos.write(response);
			fos.close();
			} catch (IOException e) {}
		
		return pathToSaveFile;
	}
}
