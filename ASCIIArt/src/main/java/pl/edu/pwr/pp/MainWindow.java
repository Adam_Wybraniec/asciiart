package pl.edu.pwr.pp;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JFileChooser;
import java.awt.Image;
import java.lang.String;
import java.net.URL;
import javax.imageio.ImageIO;
import java.io.IOException;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.image.BufferedImage;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class MainWindow {

	public
		String filePath;
		String urlPath;	
	
	private
		JFrame frameMain;
		JPanel panelButtons;
		JPanel panelImage;
		JLabel lblImage;
		DialogWindow newWindow;
		JButton btnWczytajObraz;
		JButton btnZapiszAscii;
		JComboBox<String> wyborJakosciBox = new JComboBox<>();
		
	
		enum TypAkcji {
		PGMDisk,PGMUrl,notPGMDisk,notPGMUrl
		}
		TypAkcji typAkcji;
	
	public MainWindow() {
		initialize();		
	}
		
	public static void main(String[] args) {			
		MainWindow windowMain = new MainWindow();
		windowMain.frameMain.setVisible(true);		
		windowMain.btnWczytajObrazClick(windowMain);
		windowMain.btnZapiszAsciiClick();				
	}
		
	private void btnWczytajObrazClick(MainWindow window) {			
		btnWczytajObraz.addActionListener(e -> {
				newWindow = new DialogWindow(window);
				newWindow.setVisible(true);		
			
		});		
	}

	
	private void btnZapiszAsciiClick(){		
		btnZapiszAscii.addActionListener(e -> {
				char[][] ascii = null;
				
				if (typAkcji==TypAkcji.notPGMDisk) {
					int [][]tabliczka= readNotPGMAndConvertToGreyScale();
					ascii = ImageConverter.intensitiesToAscii(tabliczka,wyborJakosciBox.getSelectedItem().toString());
				} else if (typAkcji==TypAkcji.notPGMUrl) {
					int [][]tabliczka= readNotPGMAndConvertToGreyScale();
					ascii = ImageConverter.intensitiesToAscii(tabliczka,wyborJakosciBox.getSelectedItem().toString());	
					
				} else if (typAkcji==TypAkcji.PGMDisk) {
					ascii = ImageConverter.intensitiesToAscii(newWindow.intensities,wyborJakosciBox.getSelectedItem().toString());			
				}
								
				JFileChooser chooser = new JFileChooser();
				int retrival = chooser.showSaveDialog(null);
				if (retrival == JFileChooser.APPROVE_OPTION) {
					ImageFileWriter imageFileWriter = new ImageFileWriter();
					imageFileWriter.saveToTxtFile(ascii,chooser.getSelectedFile()+".txt");
				
			}});	
	}
	
	private int[][] readNotPGMAndConvertToGreyScale(){
		ImageFileReader imageFileReader = new ImageFileReader();
		BufferedImage image = imageFileReader.readnotPGMAndReturnImage(typAkcji, filePath, urlPath);

		ImageConverter imageConverter = new ImageConverter();	
		int tablicaSzarosci[][]=imageConverter.convertRGBToGreyScale(image);

		return tablicaSzarosci;
	}		
	
	public void displayPGMFromDisk() {
		Image image = newWindow.image;			
		lblImage.setIcon(ResizeImage.resizeIcon(new ImageIcon(image),lblImage.getWidth(),lblImage.getHeight()));       
        lblImage.setText("");
        panelImage.add(lblImage);	
        panelImage.revalidate();
        panelImage.repaint();
        btnZapiszAscii.setEnabled(true);
        typAkcji=TypAkcji.PGMDisk;
	}

	public void displayPGMFromURL() {
		URL url=null;		
		
		try {
			url = new URL(urlPath);	
		} catch (java.net.MalformedURLException e) {
        	MessageBoxy.infoBox("Zly URL","Blad");
        	}	
		DownloadImages downloadImages = new DownloadImages();	
		filePath=downloadImages.DownloadPGMFileFromURL(url);	
	}	
		
	public void displayNotPGMFromDisk() {
		lblImage.setIcon(ResizeImage.resizeIcon(new ImageIcon(filePath),lblImage.getWidth(),lblImage.getHeight()));
		lblImage.setText("");
		panelImage.add(lblImage);	
		panelImage.revalidate();
		panelImage.repaint();
		btnZapiszAscii.setEnabled(true);
		typAkcji=TypAkcji.notPGMDisk;
	}
	
	public void displayNotPGMFromURL() {
		try {
			URL url = new URL(urlPath);		
            Image image = ImageIO.read(url);       
            lblImage.setIcon(ResizeImage.resizeIcon(new ImageIcon(image),lblImage.getWidth(),lblImage.getHeight()));
            lblImage.setText("");
            panelImage.add(lblImage);	
            panelImage.revalidate();
            panelImage.repaint();
            btnZapiszAscii.setEnabled(true);
            typAkcji=TypAkcji.notPGMUrl;
        } catch (java.net.MalformedURLException e) {
        	MessageBoxy.infoBox("Zly URL","Blad");
        } catch (IOException e) {
        }	
	}

	private void initialize() {
		frameMain = new JFrame("ASCIIArt");
		frameMain.setBounds(100, 100, 545, 381);
		frameMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameMain.getContentPane().setLayout(null);

		panelButtons = new JPanel();
		panelButtons.setBounds(0, 0, 167, 331);
		frameMain.getContentPane().add(panelButtons);
		panelButtons.setLayout(null);
					
		btnWczytajObraz = new JButton("Wczytaj obraz");
		btnWczytajObraz.setBounds(10, 28, 145, 40);
		panelButtons.add(btnWczytajObraz);
		
		btnZapiszAscii = new JButton("Zapisz Ascii");
		btnZapiszAscii.setEnabled(false);	
		btnZapiszAscii.setBounds(10, 189, 145, 40);
		panelButtons.add(btnZapiszAscii);
			
		wyborJakosciBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Niska", "Wysoka"}));
		wyborJakosciBox.setToolTipText("");
		wyborJakosciBox.setBounds(21, 158, 122, 20);
		panelButtons.add(wyborJakosciBox);
		
		panelImage = new JPanel();
		panelImage.setBounds(166, 0, 363, 342);
		frameMain.getContentPane().add(panelImage);
		panelImage.setLayout(null);
		
		lblImage = new JLabel("NO IMAGE");
		lblImage.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblImage.setHorizontalAlignment(SwingConstants.CENTER);
		lblImage.setBounds(10, 11, 343, 320);
		panelImage.add(lblImage);	
	}
}

