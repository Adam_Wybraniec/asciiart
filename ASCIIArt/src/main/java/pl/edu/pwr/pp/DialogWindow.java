package pl.edu.pwr.pp;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFileChooser;
import java.io.File;
import java.net.URISyntaxException;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import java.awt.Color;


public class DialogWindow extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private 
		MainWindow windowMain;
		JPanel contentPanel;
		JPanel dlgPanel;
		JPanel buttonPanel;
		JButton btnOK;
		JButton btnCancel;
		JButton btnChooseFile;
		JTextPane textFilePath;
		JTextPane textURL;
		JRadioButton rdbtnFromDisk;
		JRadioButton rdbtnFromURL;
	
	
	public 
		BufferedImage image;
		int[][] intensities;

	public void main() {
			DialogWindow windowDialog = new DialogWindow(windowMain);
			windowDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			windowDialog.setVisible(true);
	}
	
	public DialogWindow(MainWindow window) {
		windowMain=window;
		initialize();
		rdbtnFromURLListener();
		rdbtnFromDiskListener();
		btnOKClick();
		btnCancelClick();
		btnChooseFileClick();
	}
	
	private void rdbtnFromURLListener(){
			rdbtnFromURL.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textURL.setEditable(true);
				btnChooseFile.setEnabled(false);
			}
		});
	}
	
	private void rdbtnFromDiskListener(){
			rdbtnFromDisk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnChooseFile.setEnabled(true);
				textURL.setEditable(false);
			}
		});
	}
	
	void btnCancelClick(){		
		btnCancel.addActionListener(e -> {
				windowMain.urlPath="";
				dispose();
		});	
	}
	
	void btnOKClick(){
		btnOK.addActionListener(e -> {
			
				if (rdbtnFromDisk.isSelected()) {
					windowMain.filePath=textFilePath.getText();
					///////////////////////////
					// Jeżeli nie PGM z dysku//		
					///////////////////////////
					if (!windowMain.filePath.toLowerCase().contains(".pgm")) {
						windowMain.displayNotPGMFromDisk();
						dispose();
					
					///////////////////////
					// Jeżeli PGM z dysku//		
					///////////////////////
					} else if (windowMain.filePath.toLowerCase().contains(".pgm")) {
						ImageFileReader imageFileReader = new ImageFileReader();
						ImageConverter imageConverter = new ImageConverter();						
						image = imageConverter.handlePGMImage(windowMain.filePath,intensities);
						try {
							intensities = imageFileReader.readPgmFile(windowMain.filePath);
						} catch (URISyntaxException e1) {
						}
						windowMain.displayPGMFromDisk();					
						dispose(); 
					} 
				}
				else if (rdbtnFromURL.isSelected()) {					
					windowMain.urlPath=textURL.getText();
					/////////////////////
					// Jeżeli PGM z URL//		
					/////////////////////
					if (windowMain.urlPath.toLowerCase().contains(".pgm")){
						ImageFileReader imageFileReader = new ImageFileReader();						
						ImageConverter imageConverter = new ImageConverter();	
						windowMain.displayPGMFromURL();	
						image = imageConverter.handlePGMImage(windowMain.filePath,intensities);
						try {
							intensities = imageFileReader.readPgmFile(windowMain.filePath);
						} catch (URISyntaxException e1) {
						}
						windowMain.displayPGMFromDisk();					
						dispose();
					/////////////////////////
					// Jeżeli nie PGM z URL//		
					/////////////////////////
					} else if (!windowMain.urlPath.toLowerCase().contains(".pgm")){
					windowMain.displayNotPGMFromURL();
				    dispose();
					}
			
		}});
	}
	
	void btnChooseFileClick(){

		btnChooseFile.addActionListener(e -> {
			
				JFileChooser fc = new JFileChooser();
				DownloadImages downloadImages = new DownloadImages();
				
				fc.setCurrentDirectory(new File(downloadImages.resourcesDirectory.getAbsolutePath()));
				int result = fc.showOpenDialog(new JFrame());
				if (result == JFileChooser.APPROVE_OPTION) {
					final File selectedFile = fc.getSelectedFile();			
					windowMain.filePath=selectedFile.getAbsolutePath();
					textFilePath.setText(windowMain.filePath);
					windowMain.filePath=textFilePath.getText();
				}		
			
		});	
		
	}
	
	private void initialize() {
		contentPanel = new JPanel();
		dlgPanel = new JPanel();
		buttonPanel = new JPanel();
		
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
	
		dlgPanel.setBounds(10, 11, 414, 206);
		contentPanel.add(dlgPanel);
		dlgPanel.setLayout(null);
		
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
			
		btnChooseFile = new JButton("Wybierz plik...");
		btnChooseFile.setEnabled(false);
		btnChooseFile.setBounds(155, 11, 147, 43);
		dlgPanel.add(btnChooseFile);
		
		rdbtnFromDisk = new JRadioButton("Z dysku");
		rdbtnFromDisk.setBounds(40, 65, 109, 23);
		dlgPanel.add(rdbtnFromDisk);
		
		
		rdbtnFromURL = new JRadioButton("Z URL");		
		rdbtnFromURL.setBounds(40, 123, 109, 23);
		dlgPanel.add(rdbtnFromURL);
			
		ButtonGroup bG = new ButtonGroup();
		bG.add(rdbtnFromDisk);
		bG.add(rdbtnFromURL);
		
		textFilePath = new JTextPane();
		textFilePath.setBackground(Color.WHITE);
		textFilePath.setEnabled(false);
		textFilePath.setBounds(155, 65, 174, 20);
		dlgPanel.add(textFilePath);
		
		textURL = new JTextPane();
		textURL.setEditable(false);
		textURL.setBackground(Color.WHITE);
		textURL.setBounds(155, 123, 174, 23);
		dlgPanel.add(textURL);
				
		
		btnOK = new JButton("OK");	
		btnOK.setActionCommand("OK");
		buttonPanel.add(btnOK);
		getRootPane().setDefaultButton(btnOK);
		
		
		btnCancel = new JButton("Cancel");
		btnCancel.setActionCommand("Cancel");	
		buttonPanel.add(btnCancel);
	}
	
}
