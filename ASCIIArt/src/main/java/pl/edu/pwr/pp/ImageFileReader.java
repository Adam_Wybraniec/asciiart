package pl.edu.pwr.pp;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import pl.edu.pwr.pp.MainWindow.TypAkcji;

public class ImageFileReader {
		int wysokosc=0;
		int szerokosc=0;
	/**
	 * Metoda czyta plik pgm i zwraca tablicę odcieni szarości.
	 * @param fileName nazwa pliku pgm
	 * @return tablica odcieni szarości odczytanych z pliku
	 * @throws URISyntaxException jeżeli plik nie istnieje
	 */
		
	public BufferedImage readnotPGMAndReturnImage(TypAkcji typAkcji,String filePath,String urlPath){	
		BufferedImage image = null;
		if (typAkcji==TypAkcji.notPGMDisk) {
			try {
				image=ImageIO.read(new File(filePath));
			}catch (IOException e) {
			}
		} else if (typAkcji==TypAkcji.notPGMUrl) {
			try {
				URL url = new URL(urlPath);		
				image = ImageIO.read(url);
            } catch (java.net.MalformedURLException e) {
        	MessageBoxy.infoBox("Zly URL","Blad");
        } catch (IOException e) {
			}	           
		}	
		return image;
	}
		
		
		
		
		
	
	public int[][] readPgmFile(String fileName) throws URISyntaxException {
		szerokosc = 0;
		wysokosc = 0;
		int[][] intensities = null;

		//Path path = this.readPgmFile(fileName)
		//Path path = this.getPathToFile(fileName);
		Path path= Paths.get(fileName);

		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String wczytanaLinia[];
			
			//String numerMagiczny=reader.readLine();		
			reader.readLine();	
			//String komentarz=reader.readLine();
			reader.readLine();
			wczytanaLinia=reader.readLine().split(" ");			
			szerokosc = Integer.parseInt(wczytanaLinia[0]);
			wysokosc = Integer.parseInt(wczytanaLinia[1]);				
			//int najwyzsza=Integer.parseInt(reader.readLine());
			reader.readLine();
			// inicjalizacja tablicy na odcienie szarości
			intensities = new int[wysokosc][];

			for (int i = 0; i < wysokosc; i++) {
				intensities[i] = new int[szerokosc];
			}

			// kolejne linijki pliku pgm zawierają odcienie szarości kolejnych
			// pikseli rozdzielone spacjami
			String line = null;
			int currentRow = 0;
			int currentColumn = 0;
			while ((line = reader.readLine()) != null) {
				String[] elements = line.split(" ");
				for (int i = 0; i < elements.length; i++) {
					intensities[currentRow][currentColumn] = Integer.parseInt(elements[i]);
					currentColumn++;	
					
					if(currentColumn>=szerokosc){
						currentRow++;
						currentColumn=0;
					}
				}		
			}
		}catch (IOException e) {
			System.out.println("Blad jest");
		}
		return intensities;
	}
	
}
