package pl.edu.pwr.pp;

import java.awt.Image;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class ResizeImage {
	public static Icon resizeIcon(ImageIcon icon, int resizedWidth, int resizedHeight) {
	    Image img = icon.getImage();  
	    Image resizedImage;
	    if (resizedWidth<resizedHeight){
	    resizedImage = img.getScaledInstance(resizedWidth, -1,  java.awt.Image.SCALE_SMOOTH); 
	    } else {
	    resizedImage = img.getScaledInstance(-1, resizedHeight,  java.awt.Image.SCALE_SMOOTH); 
	    }    
	    return new ImageIcon(resizedImage);
	}
}
